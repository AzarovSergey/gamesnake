﻿namespace GameSnake
{
    partial class RulesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulesForm));
            this.RulesTB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // RulesTB
            // 
            this.RulesTB.Font = new System.Drawing.Font("MoolBoran", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RulesTB.Location = new System.Drawing.Point(12, 12);
            this.RulesTB.Multiline = true;
            this.RulesTB.Name = "RulesTB";
            this.RulesTB.ReadOnly = true;
            this.RulesTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RulesTB.Size = new System.Drawing.Size(197, 253);
            this.RulesTB.TabIndex = 0;
            this.RulesTB.Text = resources.GetString("RulesTB.Text");
            // 
            // RulesForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(231, 287);
            this.Controls.Add(this.RulesTB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(247, 326);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(247, 326);
            this.Name = "RulesForm";
            this.Text = "Game rules";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox RulesTB;
    }
}