﻿namespace GameSnake
{
    partial class UpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateForm));
            this.UpdateTB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // UpdateTB
            // 
            this.UpdateTB.CausesValidation = false;
            this.UpdateTB.Font = new System.Drawing.Font("Monotype Corsiva", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UpdateTB.Location = new System.Drawing.Point(12, 12);
            this.UpdateTB.Multiline = true;
            this.UpdateTB.Name = "UpdateTB";
            this.UpdateTB.ReadOnly = true;
            this.UpdateTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.UpdateTB.Size = new System.Drawing.Size(257, 361);
            this.UpdateTB.TabIndex = 0;
            this.UpdateTB.Text = resources.GetString("UpdateTB.Text");
            // 
            // UpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 385);
            this.Controls.Add(this.UpdateTB);
            this.Font = new System.Drawing.Font("Monotype Corsiva", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateForm";
            this.Text = "Update";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UpdateTB;
    }
}