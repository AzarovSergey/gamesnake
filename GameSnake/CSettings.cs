﻿using System.Drawing;

namespace GameSnake
{
    /// <summary>
    /// Класс настроек игры.
    /// </summary>
    class CSettings
    {
        /// <summary>
        /// Точка ограничевающая игровую область.
        /// </summary>
        public Point Point1 { get; set; }
        /// <summary>
        /// Точка ограничевающая игровую область.
        /// </summary>
        public Point Point2 { get; set; }
        /// <summary>
        /// Точка ограничевающая игровую область.
        /// </summary>
        public Point Point3 { get; set; }
        /// <summary>
        /// Точка ограничевающая игровую область.
        /// </summary>
        public Point Point4 { get; set; }
        /// <summary>
        /// Игровые секунды.
        /// </summary>
        private int seconds = 0;

        public int Seconds { get { return seconds; } }

        /// <summary>
        /// Игровые минуты.
        /// </summary>
        private int minutes = 0;
        /// <summary>
        /// Игровое время.
        /// </summary>
        private string game_time;
        /// <summary>
        /// Игровое время.
        /// </summary>
        public string GameTime { get { return game_time; } }
        /// <summary>
        /// Цвет игрового поля
        /// </summary>
        private Brush ColorGameMap;

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public CSettings()
        {

        }

        /// <summary>
        /// Конструктор по с заполнением ограничивающих точек.
        /// </summary>
        public CSettings(Point p1, Point p2, Point p3, Point p4, Brush color)
        {
            Point1 = p1;
            Point2 = p2;
            Point3 = p3;
            Point4 = p4;
            ColorGameMap = color;
        }

        /// <summary>
        ///  Отрисовка границ игры и заливка игровой области.
        /// </summary>
        /// <param name="gr">Поверхность рисования.</param>
        public void DrawBorders(Graphics gr)
        {
            // Размер ограничивающей точки.
            int SizePoint = 6;

            gr.FillEllipse(Brushes.Black, Point1.X - SizePoint / 2, Point1.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(Brushes.Black, Point2.X - SizePoint / 2, Point2.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(Brushes.Black, Point3.X - SizePoint / 2, Point3.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(Brushes.Black, Point4.X - SizePoint / 2, Point4.Y - SizePoint / 2, SizePoint, SizePoint);

            gr.DrawLine(new Pen(Color.Black), Point1, Point2);
            gr.DrawLine(new Pen(Color.Black), Point2, Point4);
            gr.DrawLine(new Pen(Color.Black), Point4, Point3);
            gr.DrawLine(new Pen(Color.Black), Point3, Point1);

            gr.FillRectangle(ColorGameMap, Point3.X, Point3.Y, Point4.X - Point3.X, Point1.Y - Point3.Y);


        }

        /// <summary>
        /// Расчёт времени игры.
        /// </summary>
        /// <returns>Количество сыгранных минут и секунд.</returns>
        public string TimeGame ()
        {
            seconds ++;
            if (seconds == 60)
            {
                seconds = 0;
                minutes ++;
            }
            game_time = minutes + ":" + seconds;
            return game_time;
        }
    }
}
