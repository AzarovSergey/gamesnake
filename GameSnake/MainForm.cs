﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GameSnake
{
    /// <summary>
    /// Главная форма.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Инициализация компонентов.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            PlayBT.Enabled = false;
            StopBT.Enabled = false;
            GameTimer.Interval = 1;// Snake.Speed;
            
            this.KeyDown += new KeyEventHandler(DirectionMovement);
            StopBT.KeyDown += new KeyEventHandler(DirectionMovement);
            PlayBT.KeyDown += new KeyEventHandler(DirectionMovement);
            NewGameBT.KeyDown += new KeyEventHandler(DirectionMovement);
           
            

            // Кнопка правила игры.
            ToolStripMenuItem Rules = new ToolStripMenuItem("Rules game");
            Menu.Items.Add(Rules);
            // Копка обновления.
            ToolStripMenuItem Update = new ToolStripMenuItem("Updates");
            Menu.Items.Add(Update);

            Rules.Click += RulesClick;
            Update.Click += UpdateClick;

            this.Focus();
        }

        /// <summary>
        /// Выбор цвета головы змеи.
        /// </summary>
        /// <returns>Цвет кисти.</returns>
        private Brush ColorHead()
        {
            if(HeadBlackRB.Checked)
            {
                return Brushes.Black;
            }
            if (HeadBlueRB.Checked)
            {
                return Brushes.Blue;
            }
            if (HeadGreenRB.Checked)
            {
                return Brushes.Green;
            }
            return Brushes.Black;
        }

        /// <summary>
        /// Выбор цвета хвоста змеи.
        /// </summary>
        /// <returns>Цвет кисти.</returns>
        private Brush ColorTail()
        {
            if(TailBlackRB.Checked)
            {
                return Brushes.Black;
            }
            if (TailBlueRB.Checked)
            {
                return Brushes.Blue;
            }
            if (TailGreenRB.Checked)
            {
                return Brushes.Green;
            }
            return Brushes.Green;
        }

        /// <summary>
        /// Выбор цвета игрового поля.
        /// </summary>
        /// <returns>Цвет кисти.</returns>
        private Brush ColorGameMap()
        {
            if (MapLBlueRB.Checked)
            {
                pictureBox1.BackColor = Color.LightBlue;
                return Brushes.LightBlue;
            }
            if (MapLGrayRB.Checked)
            {
                pictureBox1.BackColor = Color.LightGray;
                return Brushes.LightGray;
            }
            if (MapLGreenRB.Checked)
            {
                pictureBox1.BackColor = Color.LightSeaGreen;
                return Brushes.LightSeaGreen;
            }
            if (MapLYellowRB.Checked)
            {
                pictureBox1.BackColor = Color.LightGoldenrodYellow;
                return Brushes.LightGoldenrodYellow;
            }
            return Brushes.LightGray;
        }

        /// <summary>
        /// Выбор скорости змеи.
        /// </summary>
        /// <returns></returns>
        private int SpeedSnake()
        {
            if(LowSpeedRB.Checked)
            {
                return 1;
            }
            if (AverageSpeedRB.Checked)
            {
                return 2;
            }
            if (HighSpeedRB.Checked)
            {
                return 3;
            }
            return  1;

        }

        private void SizeGameMap()
        {
            if(SmollSizeMapRB.Checked)
            {
                Offset = 140;
            }
            if(NormalSizeMapRB.Checked)
            {
                Offset = 70;
            }
            if(BigSizeMapRB.Checked)
            {
                Offset = 0;
            }
        }



        /// <summary>
        /// Вызов формы с правилами игры.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RulesClick(object sender, EventArgs e)
        {
            RulesForm FormMessage = new RulesForm();
            FormMessage.ShowDialog();
        }

        /// <summary>
        /// Вызов формы с обновлениями.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateClick(object sender, EventArgs e)
        {
            UpdateForm FormMessage = new UpdateForm();
            FormMessage.ShowDialog();
        }

        /// <summary>
        /// Выбор направления движения змеи.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirectionMovement(object sender, KeyEventArgs e)
        {
            Snake.DirectionMovement(e.KeyCode.ToString());
        }

        /// <summary>
        /// Поверхность отрисовки.
        /// </summary>
        Graphics Graph;
        /// <summary>
        /// Величина смещения от края игровой карты.
        /// </summary>
        int Offset=0;
        /// <summary>
        ///  Объект для настроек игры.
        /// </summary>
        CSettings Settings = new CSettings();
        /// <summary>
        /// Объект для змейки.
        /// </summary>
        CSnake Snake = new CSnake();
        /// <summary>
        /// Объект для еды.
        /// </summary>
        CFood Food = new CFood();

       

        /// <summary>
        /// Кнопка play.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayBT_Click(object sender, EventArgs e)
        {
            Time.Start();
            GameTimer.Start();

        }


        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Проверка на то какая еда съедена.
        /// </summary>
        private void CheckEatenFood()
        {
            //Проверка на съеденную еду.
            bool CheckUsualFood = Snake.UsualFoodEaten(Food.FieldUsualFoodProperty);
            bool CheckDestroyFood = Snake.DestroyFoodEaten(Food.FieldFoodDestroyTailProperty);
            bool CheckAddFood = Snake.AddFoodEaten(Food.FieldFoodAddTailProperty);

            if (CheckUsualFood)
            {
                Food.GetNewFood(Settings.Point1, Settings.Point2, Settings.Point3, Settings.Point4, Snake.Snake, CFood.TypeFood.UsualFood, Graph);
            }
            if (CheckDestroyFood)
            {
                Food.ClearFoodDestroyTail();
            }
            if (CheckAddFood)
            {
                Food.ClearFoodAddTail();
            }
        }

        /// <summary>
        /// Таймер игры.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            MapPB.Image = new Bitmap(MapPB.Width, MapPB.Height);
            Graph = Graphics.FromImage(MapPB.Image);
            Graph.Clear(Color.White);
            Graph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            Settings.DrawBorders(Graph);

            
            Food.DrawFood(Graph);

            pictureBox1.Location = new Point(Snake.Snake[0].X + 6, Snake.Snake[0].Y+20); // разобраться с числами
            pictureBox1.Image = Image.FromFile("C:\\Users\\Сергей\\Desktop\\HeadUp.png");
            Snake.MoveSnake(Graph);
            
           

            CheckEatenFood();
            ScoreL.Text = Convert.ToString(Snake.Score);

            



            // Проверка на смерть змейки.
            bool CheckDead = Snake.DeadSnake(Settings.Point1, Settings.Point2, Settings.Point3, Settings.Point4, Food.FieldToxinProperty);
            
            if (CheckDead)
            {
                Time.Stop();
                
                
                Graph.Clear(Color.White);
                ScoreL.Text = "0";
                GameTime.Text = "0:0";
                GameTimer.Stop();
                PlayBT.Enabled = false;
                StopBT.Enabled = false;
                ColorGameMapGB.Enabled = true;
                ColorHeadGB.Enabled = true;
                ColorTailGB.Enabled = true;
                SpeedSnakeGB.Enabled = true;
                SizeGameMapGB.Enabled = true;
                MessageBox.Show( "You dead!\nYour score: "+ Snake.Score + "\nYour game time: " + Settings.GameTime, "You dead" );
            }

            



        }

        
        /// <summary>
        /// Таймер игрового времени.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick_1(object sender, EventArgs e)
        {
            GameTime.Text = Settings.TimeGame();

            Food.TimeLiveFood(Settings.Seconds,Settings.Point1, Settings.Point2, Settings.Point3, Settings.Point4, Snake.Snake, Graph);
        }

        /// <summary>
        /// Кнопка stop.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopBT_Click(object sender, EventArgs e)
        {
            PlayBT.Enabled = true;
            ColorGameMapGB.Enabled = true;
            ColorHeadGB.Enabled = true;
            ColorTailGB.Enabled = true;
            SpeedSnakeGB.Enabled = true;
            SizeGameMapGB.Enabled = true;
            GameTimer.Stop();
            Time.Stop();
        }

        /// <summary>
        /// Кнопка NewGame.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewGameBT_Click(object sender, EventArgs e)
        {
            StopBT.Enabled = true;
            ColorGameMapGB.Enabled = false;
            ColorHeadGB.Enabled = false;
            ColorTailGB.Enabled = false;
            SpeedSnakeGB.Enabled = false;
            SizeGameMapGB.Enabled = false;

            MapPB.Image = new Bitmap(MapPB.Width, MapPB.Height);
            Graph = Graphics.FromImage(MapPB.Image);
            Graph.Clear(Color.White);
            Graph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            SizeGameMap();
            
            Settings = new CSettings
                (
                new Point(Offset, MapPB.Height - Offset),
                new Point(MapPB.Width - Offset, MapPB.Height - Offset),
                new Point(Offset, Offset),
                new Point(MapPB.Width - Offset, Offset),
                ColorGameMap()
                );

            Settings.DrawBorders(Graph);

            Snake = new CSnake(new Point(MapPB.Width / 2, MapPB.Height / 2), SpeedSnake(), Graph,ColorHead(),ColorTail());

            Food.GetNewFood(Settings.Point1, Settings.Point2, Settings.Point3, Settings.Point4, Snake.Snake, CFood.TypeFood.UsualFood, Graph);
            Time.Start();
            GameTimer.Start();
        }
    }
}
