﻿namespace GameSnake
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.PlayBT = new System.Windows.Forms.Button();
            this.StopBT = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NewGameBT = new System.Windows.Forms.Button();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.GameTime = new System.Windows.Forms.Label();
            this.Time = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.ColorHeadGB = new System.Windows.Forms.GroupBox();
            this.HeadBlackRB = new System.Windows.Forms.RadioButton();
            this.HeadBlueRB = new System.Windows.Forms.RadioButton();
            this.HeadGreenRB = new System.Windows.Forms.RadioButton();
            this.ColorTailGB = new System.Windows.Forms.GroupBox();
            this.TailBlackRB = new System.Windows.Forms.RadioButton();
            this.TailBlueRB = new System.Windows.Forms.RadioButton();
            this.TailGreenRB = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.ColorGameMapGB = new System.Windows.Forms.GroupBox();
            this.MapLGreenRB = new System.Windows.Forms.RadioButton();
            this.MapLYellowRB = new System.Windows.Forms.RadioButton();
            this.MapLBlueRB = new System.Windows.Forms.RadioButton();
            this.MapLGrayRB = new System.Windows.Forms.RadioButton();
            this.SpeedSnakeGB = new System.Windows.Forms.GroupBox();
            this.HighSpeedRB = new System.Windows.Forms.RadioButton();
            this.AverageSpeedRB = new System.Windows.Forms.RadioButton();
            this.LowSpeedRB = new System.Windows.Forms.RadioButton();
            this.SizeGameMapGB = new System.Windows.Forms.GroupBox();
            this.BigSizeMapRB = new System.Windows.Forms.RadioButton();
            this.NormalSizeMapRB = new System.Windows.Forms.RadioButton();
            this.SmollSizeMapRB = new System.Windows.Forms.RadioButton();
            this.ScoreL = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MapPB = new System.Windows.Forms.PictureBox();
            this.ColorHeadGB.SuspendLayout();
            this.ColorTailGB.SuspendLayout();
            this.ColorGameMapGB.SuspendLayout();
            this.SpeedSnakeGB.SuspendLayout();
            this.SizeGameMapGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapPB)).BeginInit();
            this.SuspendLayout();
            // 
            // GameTimer
            // 
            this.GameTimer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // PlayBT
            // 
            this.PlayBT.CausesValidation = false;
            this.PlayBT.Location = new System.Drawing.Point(695, 12);
            this.PlayBT.Name = "PlayBT";
            this.PlayBT.Size = new System.Drawing.Size(89, 23);
            this.PlayBT.TabIndex = 1;
            this.PlayBT.Text = "Play";
            this.PlayBT.UseVisualStyleBackColor = true;
            this.PlayBT.Click += new System.EventHandler(this.PlayBT_Click);
            // 
            // StopBT
            // 
            this.StopBT.CausesValidation = false;
            this.StopBT.Location = new System.Drawing.Point(695, 41);
            this.StopBT.Name = "StopBT";
            this.StopBT.Size = new System.Drawing.Size(89, 23);
            this.StopBT.TabIndex = 2;
            this.StopBT.Text = "Stop";
            this.StopBT.UseVisualStyleBackColor = true;
            this.StopBT.Click += new System.EventHandler(this.StopBT_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(790, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Score:";
            // 
            // NewGameBT
            // 
            this.NewGameBT.CausesValidation = false;
            this.NewGameBT.Location = new System.Drawing.Point(695, 70);
            this.NewGameBT.Name = "NewGameBT";
            this.NewGameBT.Size = new System.Drawing.Size(89, 23);
            this.NewGameBT.TabIndex = 10;
            this.NewGameBT.Text = "New Game";
            this.NewGameBT.UseVisualStyleBackColor = true;
            this.NewGameBT.Click += new System.EventHandler(this.NewGameBT_Click);
            // 
            // Menu
            // 
            this.Menu.BackColor = System.Drawing.SystemColors.Control;
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(898, 24);
            this.Menu.TabIndex = 11;
            this.Menu.Text = "menuStrip1";
            // 
            // GameTime
            // 
            this.GameTime.AutoSize = true;
            this.GameTime.Location = new System.Drawing.Point(857, 27);
            this.GameTime.Name = "GameTime";
            this.GameTime.Size = new System.Drawing.Size(22, 13);
            this.GameTime.TabIndex = 12;
            this.GameTime.Text = "0:0";
            // 
            // Time
            // 
            this.Time.Interval = 1000;
            this.Time.Tick += new System.EventHandler(this.Timer_Tick_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(790, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Game Time:";
            // 
            // ColorHeadGB
            // 
            this.ColorHeadGB.Controls.Add(this.HeadBlackRB);
            this.ColorHeadGB.Controls.Add(this.HeadBlueRB);
            this.ColorHeadGB.Controls.Add(this.HeadGreenRB);
            this.ColorHeadGB.Location = new System.Drawing.Point(695, 99);
            this.ColorHeadGB.Name = "ColorHeadGB";
            this.ColorHeadGB.Size = new System.Drawing.Size(93, 95);
            this.ColorHeadGB.TabIndex = 14;
            this.ColorHeadGB.TabStop = false;
            this.ColorHeadGB.Text = "ColorHead";
            // 
            // HeadBlackRB
            // 
            this.HeadBlackRB.AutoSize = true;
            this.HeadBlackRB.Checked = true;
            this.HeadBlackRB.Location = new System.Drawing.Point(10, 19);
            this.HeadBlackRB.Name = "HeadBlackRB";
            this.HeadBlackRB.Size = new System.Drawing.Size(52, 17);
            this.HeadBlackRB.TabIndex = 3;
            this.HeadBlackRB.TabStop = true;
            this.HeadBlackRB.Text = "Black";
            this.HeadBlackRB.UseVisualStyleBackColor = true;
            // 
            // HeadBlueRB
            // 
            this.HeadBlueRB.AutoSize = true;
            this.HeadBlueRB.Location = new System.Drawing.Point(10, 65);
            this.HeadBlueRB.Name = "HeadBlueRB";
            this.HeadBlueRB.Size = new System.Drawing.Size(46, 17);
            this.HeadBlueRB.TabIndex = 2;
            this.HeadBlueRB.Text = "Blue";
            this.HeadBlueRB.UseVisualStyleBackColor = true;
            // 
            // HeadGreenRB
            // 
            this.HeadGreenRB.AutoSize = true;
            this.HeadGreenRB.Location = new System.Drawing.Point(10, 42);
            this.HeadGreenRB.Name = "HeadGreenRB";
            this.HeadGreenRB.Size = new System.Drawing.Size(54, 17);
            this.HeadGreenRB.TabIndex = 1;
            this.HeadGreenRB.Text = "Green";
            this.HeadGreenRB.UseVisualStyleBackColor = true;
            // 
            // ColorTailGB
            // 
            this.ColorTailGB.Controls.Add(this.TailBlackRB);
            this.ColorTailGB.Controls.Add(this.TailBlueRB);
            this.ColorTailGB.Controls.Add(this.TailGreenRB);
            this.ColorTailGB.Location = new System.Drawing.Point(798, 99);
            this.ColorTailGB.Name = "ColorTailGB";
            this.ColorTailGB.Size = new System.Drawing.Size(93, 95);
            this.ColorTailGB.TabIndex = 15;
            this.ColorTailGB.TabStop = false;
            this.ColorTailGB.Text = "ColorTail";
            // 
            // TailBlackRB
            // 
            this.TailBlackRB.AutoSize = true;
            this.TailBlackRB.Location = new System.Drawing.Point(10, 19);
            this.TailBlackRB.Name = "TailBlackRB";
            this.TailBlackRB.Size = new System.Drawing.Size(52, 17);
            this.TailBlackRB.TabIndex = 3;
            this.TailBlackRB.TabStop = true;
            this.TailBlackRB.Text = "Black";
            this.TailBlackRB.UseVisualStyleBackColor = true;
            // 
            // TailBlueRB
            // 
            this.TailBlueRB.AutoSize = true;
            this.TailBlueRB.Location = new System.Drawing.Point(10, 65);
            this.TailBlueRB.Name = "TailBlueRB";
            this.TailBlueRB.Size = new System.Drawing.Size(46, 17);
            this.TailBlueRB.TabIndex = 2;
            this.TailBlueRB.Text = "Blue";
            this.TailBlueRB.UseVisualStyleBackColor = true;
            // 
            // TailGreenRB
            // 
            this.TailGreenRB.AutoSize = true;
            this.TailGreenRB.Checked = true;
            this.TailGreenRB.Location = new System.Drawing.Point(10, 42);
            this.TailGreenRB.Name = "TailGreenRB";
            this.TailGreenRB.Size = new System.Drawing.Size(54, 17);
            this.TailGreenRB.TabIndex = 1;
            this.TailGreenRB.TabStop = true;
            this.TailGreenRB.Text = "Green";
            this.TailGreenRB.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(10, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(54, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Green";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(10, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(46, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Blue";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(10, 42);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(54, 17);
            this.radioButton5.TabIndex = 1;
            this.radioButton5.Text = "Green";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(10, 65);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(46, 17);
            this.radioButton4.TabIndex = 2;
            this.radioButton4.Text = "Blue";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // ColorGameMapGB
            // 
            this.ColorGameMapGB.Controls.Add(this.MapLGreenRB);
            this.ColorGameMapGB.Controls.Add(this.MapLYellowRB);
            this.ColorGameMapGB.Controls.Add(this.MapLBlueRB);
            this.ColorGameMapGB.Controls.Add(this.MapLGrayRB);
            this.ColorGameMapGB.Location = new System.Drawing.Point(695, 200);
            this.ColorGameMapGB.Name = "ColorGameMapGB";
            this.ColorGameMapGB.Size = new System.Drawing.Size(196, 76);
            this.ColorGameMapGB.TabIndex = 16;
            this.ColorGameMapGB.TabStop = false;
            this.ColorGameMapGB.Text = "ColorGameMap";
            // 
            // MapLGreenRB
            // 
            this.MapLGreenRB.AutoSize = true;
            this.MapLGreenRB.Location = new System.Drawing.Point(98, 42);
            this.MapLGreenRB.Name = "MapLGreenRB";
            this.MapLGreenRB.Size = new System.Drawing.Size(80, 17);
            this.MapLGreenRB.TabIndex = 2;
            this.MapLGreenRB.Text = "Light Green";
            this.MapLGreenRB.UseVisualStyleBackColor = true;
            // 
            // MapLYellowRB
            // 
            this.MapLYellowRB.AutoSize = true;
            this.MapLYellowRB.Location = new System.Drawing.Point(10, 42);
            this.MapLYellowRB.Name = "MapLYellowRB";
            this.MapLYellowRB.Size = new System.Drawing.Size(82, 17);
            this.MapLYellowRB.TabIndex = 1;
            this.MapLYellowRB.Text = "Light Yellow";
            this.MapLYellowRB.UseVisualStyleBackColor = true;
            // 
            // MapLBlueRB
            // 
            this.MapLBlueRB.AutoSize = true;
            this.MapLBlueRB.Location = new System.Drawing.Point(98, 19);
            this.MapLBlueRB.Name = "MapLBlueRB";
            this.MapLBlueRB.Size = new System.Drawing.Size(72, 17);
            this.MapLBlueRB.TabIndex = 3;
            this.MapLBlueRB.Text = "Light Blue";
            this.MapLBlueRB.UseVisualStyleBackColor = true;
            // 
            // MapLGrayRB
            // 
            this.MapLGrayRB.AutoSize = true;
            this.MapLGrayRB.Checked = true;
            this.MapLGrayRB.Location = new System.Drawing.Point(10, 19);
            this.MapLGrayRB.Name = "MapLGrayRB";
            this.MapLGrayRB.Size = new System.Drawing.Size(73, 17);
            this.MapLGrayRB.TabIndex = 0;
            this.MapLGrayRB.TabStop = true;
            this.MapLGrayRB.Text = "Light Gray";
            this.MapLGrayRB.UseVisualStyleBackColor = true;
            // 
            // SpeedSnakeGB
            // 
            this.SpeedSnakeGB.Controls.Add(this.HighSpeedRB);
            this.SpeedSnakeGB.Controls.Add(this.AverageSpeedRB);
            this.SpeedSnakeGB.Controls.Add(this.LowSpeedRB);
            this.SpeedSnakeGB.Location = new System.Drawing.Point(695, 282);
            this.SpeedSnakeGB.Name = "SpeedSnakeGB";
            this.SpeedSnakeGB.Size = new System.Drawing.Size(196, 55);
            this.SpeedSnakeGB.TabIndex = 18;
            this.SpeedSnakeGB.TabStop = false;
            this.SpeedSnakeGB.Text = "SpeedSnake";
            // 
            // HighSpeedRB
            // 
            this.HighSpeedRB.AutoSize = true;
            this.HighSpeedRB.Location = new System.Drawing.Point(123, 19);
            this.HighSpeedRB.Name = "HighSpeedRB";
            this.HighSpeedRB.Size = new System.Drawing.Size(47, 17);
            this.HighSpeedRB.TabIndex = 2;
            this.HighSpeedRB.Text = "High";
            this.HighSpeedRB.UseVisualStyleBackColor = true;
            // 
            // AverageSpeedRB
            // 
            this.AverageSpeedRB.AutoSize = true;
            this.AverageSpeedRB.Location = new System.Drawing.Point(57, 19);
            this.AverageSpeedRB.Name = "AverageSpeedRB";
            this.AverageSpeedRB.Size = new System.Drawing.Size(65, 17);
            this.AverageSpeedRB.TabIndex = 1;
            this.AverageSpeedRB.Text = "Average";
            this.AverageSpeedRB.UseVisualStyleBackColor = true;
            // 
            // LowSpeedRB
            // 
            this.LowSpeedRB.AutoSize = true;
            this.LowSpeedRB.Checked = true;
            this.LowSpeedRB.Location = new System.Drawing.Point(6, 19);
            this.LowSpeedRB.Name = "LowSpeedRB";
            this.LowSpeedRB.Size = new System.Drawing.Size(45, 17);
            this.LowSpeedRB.TabIndex = 0;
            this.LowSpeedRB.TabStop = true;
            this.LowSpeedRB.Text = "Low";
            this.LowSpeedRB.UseVisualStyleBackColor = true;
            // 
            // SizeGameMapGB
            // 
            this.SizeGameMapGB.Controls.Add(this.BigSizeMapRB);
            this.SizeGameMapGB.Controls.Add(this.NormalSizeMapRB);
            this.SizeGameMapGB.Controls.Add(this.SmollSizeMapRB);
            this.SizeGameMapGB.Location = new System.Drawing.Point(695, 343);
            this.SizeGameMapGB.Name = "SizeGameMapGB";
            this.SizeGameMapGB.Size = new System.Drawing.Size(196, 51);
            this.SizeGameMapGB.TabIndex = 19;
            this.SizeGameMapGB.TabStop = false;
            this.SizeGameMapGB.Text = "SizeGameMap";
            // 
            // BigSizeMapRB
            // 
            this.BigSizeMapRB.AutoSize = true;
            this.BigSizeMapRB.Location = new System.Drawing.Point(123, 19);
            this.BigSizeMapRB.Name = "BigSizeMapRB";
            this.BigSizeMapRB.Size = new System.Drawing.Size(40, 17);
            this.BigSizeMapRB.TabIndex = 22;
            this.BigSizeMapRB.Text = "Big\r\n";
            this.BigSizeMapRB.UseVisualStyleBackColor = true;
            // 
            // NormalSizeMapRB
            // 
            this.NormalSizeMapRB.AutoSize = true;
            this.NormalSizeMapRB.Location = new System.Drawing.Point(57, 19);
            this.NormalSizeMapRB.Name = "NormalSizeMapRB";
            this.NormalSizeMapRB.Size = new System.Drawing.Size(58, 17);
            this.NormalSizeMapRB.TabIndex = 21;
            this.NormalSizeMapRB.Text = "Normal";
            this.NormalSizeMapRB.UseVisualStyleBackColor = true;
            // 
            // SmollSizeMapRB
            // 
            this.SmollSizeMapRB.AutoSize = true;
            this.SmollSizeMapRB.Checked = true;
            this.SmollSizeMapRB.Location = new System.Drawing.Point(4, 19);
            this.SmollSizeMapRB.Name = "SmollSizeMapRB";
            this.SmollSizeMapRB.Size = new System.Drawing.Size(50, 17);
            this.SmollSizeMapRB.TabIndex = 20;
            this.SmollSizeMapRB.TabStop = true;
            this.SmollSizeMapRB.Text = "Smoll";
            this.SmollSizeMapRB.UseVisualStyleBackColor = true;
            // 
            // ScoreL
            // 
            this.ScoreL.AutoSize = true;
            this.ScoreL.Location = new System.Drawing.Point(830, 12);
            this.ScoreL.Name = "ScoreL";
            this.ScoreL.Size = new System.Drawing.Size(13, 13);
            this.ScoreL.TabIndex = 20;
            this.ScoreL.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(705, 400);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // MapPB
            // 
            this.MapPB.Location = new System.Drawing.Point(12, 27);
            this.MapPB.Name = "MapPB";
            this.MapPB.Size = new System.Drawing.Size(677, 411);
            this.MapPB.TabIndex = 0;
            this.MapPB.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ScoreL);
            this.Controls.Add(this.SizeGameMapGB);
            this.Controls.Add(this.SpeedSnakeGB);
            this.Controls.Add(this.ColorGameMapGB);
            this.Controls.Add(this.ColorTailGB);
            this.Controls.Add(this.ColorHeadGB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GameTime);
            this.Controls.Add(this.NewGameBT);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StopBT);
            this.Controls.Add(this.PlayBT);
            this.Controls.Add(this.MapPB);
            this.Controls.Add(this.Menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.Menu;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(914, 489);
            this.MinimumSize = new System.Drawing.Size(914, 489);
            this.Name = "MainForm";
            this.Text = "Snake";
            this.ColorHeadGB.ResumeLayout(false);
            this.ColorHeadGB.PerformLayout();
            this.ColorTailGB.ResumeLayout(false);
            this.ColorTailGB.PerformLayout();
            this.ColorGameMapGB.ResumeLayout(false);
            this.ColorGameMapGB.PerformLayout();
            this.SpeedSnakeGB.ResumeLayout(false);
            this.SpeedSnakeGB.PerformLayout();
            this.SizeGameMapGB.ResumeLayout(false);
            this.SizeGameMapGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapPB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MapPB;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.Button PlayBT;
        private System.Windows.Forms.Button StopBT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button NewGameBT;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.Label GameTime;
        private System.Windows.Forms.Timer Time;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox ColorHeadGB;
        private System.Windows.Forms.RadioButton HeadBlueRB;
        private System.Windows.Forms.RadioButton HeadGreenRB;
        private System.Windows.Forms.GroupBox ColorTailGB;
        private System.Windows.Forms.RadioButton TailBlueRB;
        private System.Windows.Forms.RadioButton TailGreenRB;
        
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton HeadBlackRB;
        private System.Windows.Forms.GroupBox ColorGameMapGB;
        private System.Windows.Forms.RadioButton MapLGreenRB;
        private System.Windows.Forms.RadioButton MapLYellowRB;
        private System.Windows.Forms.RadioButton MapLBlueRB;
        private System.Windows.Forms.RadioButton MapLGrayRB;
        private System.Windows.Forms.RadioButton TailBlackRB;
        private System.Windows.Forms.GroupBox SpeedSnakeGB;
        private System.Windows.Forms.RadioButton HighSpeedRB;
        private System.Windows.Forms.RadioButton AverageSpeedRB;
        private System.Windows.Forms.RadioButton LowSpeedRB;
        private System.Windows.Forms.GroupBox SizeGameMapGB;
        private System.Windows.Forms.RadioButton BigSizeMapRB;
        private System.Windows.Forms.RadioButton NormalSizeMapRB;
        private System.Windows.Forms.RadioButton SmollSizeMapRB;
        private System.Windows.Forms.Label ScoreL;
        public System.Windows.Forms.PictureBox pictureBox1;
    }
}

