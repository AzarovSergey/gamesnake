﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Media;
namespace GameSnake
{
    class CSnake 
    {
        /// <summary>
        ///  Шаг движения змеи по координате Х.
        /// </summary>
        private int stepX = 0;
        /// <summary>
        ///  Шаг движения змеи по координате Y.
        /// </summary>
        private int stepY = 0;
        /// <summary>
        /// Скорость движения змеи.
        /// </summary>
        private int speed;
        /// <summary>
        /// Предыдущие направление движения змеи.
        /// </summary>
        private string PrevMovement = "";
        /// <summary>
        /// Голова змеи.
        /// </summary>
        private Point Head;
        /// <summary>
        /// Хвост змеи.
        /// </summary>
        private Point Tail;
        /// <summary>
        /// Скорость змеи.
        /// </summary>
        public int Speed { get { return speed; } set { speed = value; } }
        /// <summary>
        /// Очки за съеденную еду.
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// Целая змея.
        /// </summary>
        public List<Point> Snake = new List<Point>();
        /// <summary>
        /// Цвет головы змеи;
        /// </summary>
        private Brush ColorHead;
        /// <summary>
        /// Цвет хвоста змеи.
        /// </summary>
        private Brush ColorTail;

        

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public CSnake()
        {

        }

        /// <summary>
        /// Конструктор с созданием головы змеи и добавление её в целую змею.
        /// </summary>
        /// <param name="hd">Голова змеи.</param>
        /// <param name="gr">Поверхность для рисования</param>
        public CSnake(Point hd, int speed, Graphics gr, Brush color_head, Brush color_tail)
        {
            Head = hd;
            Speed = speed;
            Snake.Add(Head);
            ColorHead = color_head;
            ColorTail = color_tail;
        }

        
        /// <summary>
        /// Выбор направления движения. Все направления инвертированы из-за системы координат.
        /// </summary>
        public void DirectionMovement(string str)
        {
            switch (str)
            {
                case "D":
                    if (PrevMovement != "A" && PrevMovement != "D")
                    {
                        stepX = 1;
                        stepY = 0;
                    }
                    
                    break;
                case "A":
                    if (PrevMovement != "D" && PrevMovement != "A")
                    {
                        stepX = -1;
                        stepY = 0;
                    }
                    break;
                case "W":
                    if (PrevMovement != "S" && PrevMovement != "W")
                    {
                        stepY = -1;
                        stepX = 0;
                    }
                    break;
                case "S":
                    if(PrevMovement != "W" && PrevMovement != "S")
                    {
                        stepY = 1;
                        stepX = 0;
                    }
                    break;
            }
            PrevMovement = str;
        }

        /// <summary>
        /// Обновляем координаты змеи с учётом движения.
        /// </summary>
        /// <param name="gr">Поверхность рисования.</param>
        public void MoveSnake(Graphics gr)
        {
            // Размер точки змеи.
            int SizePoint = 10;
            for (int i = Snake.Count-1; i >= 1; i--)
            {
                Snake[i] = Snake[i - 1];
            }
            Snake[0] = new Point(Snake[0].X+stepX*speed, Snake[0].Y + stepY*speed);
            
            for (int i = 1; i < Snake.Count; i++)
            {
                gr.FillEllipse(ColorTail, Snake[i].X - SizePoint / 2, Snake[i].Y - SizePoint / 2, SizePoint, SizePoint);
            }
            gr.FillEllipse(ColorHead, Snake[0].X - SizePoint / 2, Snake[0].Y - SizePoint / 2, SizePoint, SizePoint);
            
        }

        /// <summary>
        /// Проверка на съеденную обычную еду.
        /// </summary>
        /// <param name="fieldFood">Область еды.</param>
        /// <returns>Истина если съеденна.</returns>
        public bool UsualFoodEaten(List<Point> fieldFood)
        {
            // Количество игровых очков за съеденную еду.
            int CountScore = 10;

            foreach (Point el in fieldFood)
            {
                if (Snake[0] == el)
                {
                    // Колчиство точек в хвосте за 1 съеденную еду.
                    int CountTailPoint = 5;
                    for (int i = 0; i < CountTailPoint; i++)
                    {
                        Tail = new Point(Snake[Snake.Count - 1].X - stepX*speed, Snake[Snake.Count - 1].Y - stepY*speed);
                        Snake.Add(Tail);
                    }

                    AddScore(CountScore);

                   // PlayWav(Properties.Resources.belch);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Проверка на съеденную еду уничтожающую хчвост.
        /// </summary>
        /// <param name="fieldFood">Область еды.</param>
        /// <returns>Истина если съеденно.</returns>
        public bool DestroyFoodEaten(List<Point> fieldFood)
        {
            foreach (Point el in fieldFood)
            {
                if (Snake[0] == el)
                {
                    for (int i = Snake.Count - 1; i >= 1; i--)
                    {
                        Snake.RemoveAt(i);
                    }
                   // PlayWav(Properties.Resources.belch);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Проверка на съеденную еду увеличивающую хвост на 6 звеньев.
        /// </summary>
        /// <param name="fieldFood">Область еды.</param>
        /// <returns>Истина если съедена.</returns>
        public bool AddFoodEaten(List<Point> fieldFood)
        {
            // Количество игровых очков за съеденную еду.
            int CountScore = 20;

            foreach (Point el in fieldFood)
            {
                if (Snake[0] == el)
                {
                    // Колчиство точек в хвосте за 1 съеденную еду.
                    int CountTailPoint = 30;
                    for (int i = 0; i < CountTailPoint; i++)
                    {
                        Tail = new Point(Snake[Snake.Count - 1].X - stepX, Snake[Snake.Count - 1].Y - stepY);
                        Snake.Add(Tail);
                    }

                    AddScore(CountScore);

                   // PlayWav(Properties.Resources.belch);
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Проверка на съеденный яд.
        /// </summary>
        /// <param name="fieldFood">Область еды.</param>
        /// <returns>Истина если съеденно.</returns>
        public bool ToxinEaten(List<Point> fieldFood)
        {
            foreach (Point el in fieldFood)
            {
                if (Snake[0] == el)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Воспроизведение игровых звуков.
        /// </summary>
        /// <param name="stream">Путь к аудио файлу.</param>
        private void PlayWav(Stream stream)
        {
            SoundPlayer SoundPlayer = new SoundPlayer(stream);
            SoundPlayer.Play();
        }

        /// <summary>
        /// Добавление игровых очков.
        /// </summary>
        /// <param name="count"></param>
        private void AddScore(int count)
        {
            Score += count;

        }

        /// <summary>
        /// Проверяем не умерла ли наша змея. (Не врезалась ли в саму себя или не ушла за границы игровой область.)
        /// </summary>
        /// <param name="p1">Точка ограничевающая игровую область.</param>
        /// <param name="p2">Точка ограничевающая игровую область.</param>
        /// <param name="p3">Точка ограничевающая игровую область.</param>
        /// <param name="p4">Точка ограничевающая игровую область.</param>
        /// <param name="fieldFood">Область яда.</param>
        /// <returns>Если змейка умерла вернёт истину иначе ложь.</returns>
        public bool DeadSnake(Point p1, Point p2, Point p3, Point p4, List<Point> fieldFood)
        {
            if (ToxinEaten(fieldFood)) return true;
            if(Snake[0].X<p3.X|| Snake[0].X>p4.X|| Snake[0].Y<p3.Y|| Snake[0].Y>p1.Y)
            {
              //  PlayWav(Properties.Resources.haha);
                return true;
            }
            for (int i =1; i<Snake.Count; i++)
            {
                if(Snake[0]==Snake[i])
                {
                   // PlayWav(Properties.Resources.fart55);
                    return true;
                }
            }
            return false;
        }

       
    }
}
