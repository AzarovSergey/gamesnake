﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace GameSnake
{
    /// <summary>
    ///  Класс для еды.
    /// </summary>
    class CFood
    {
        /// <summary>
        /// Область еды.
        /// </summary>
        private List<Point> FieldUsualFood = new List<Point>();
        /// <summary>
        /// Область еды.
        /// </summary>
        public List<Point> FieldUsualFoodProperty { get { return FieldUsualFood; } }
        /// <summary>
        /// Центр еды.
        /// </summary>
        private Point UsualFood;
        /// <summary>
        /// Область еды отрезающей хвост.
        /// </summary>
        private List<Point> FieldFoodDestroyTail = new List<Point>();
        /// <summary>
        /// Область еды отрезающей хвост.
        /// </summary>
        public List<Point> FieldFoodDestroyTailProperty { get { return FieldFoodDestroyTail; } }
        /// <summary>
        /// Центр еды уничтожающей хвост.
        /// </summary>
        private Point FoodDestroyTail = new Point(-10, -10);
        /// <summary>
        /// Область еды увеличивающий хвост на 6 звеньев хвост.
        /// </summary>
        private List<Point> FieldFoodAddTail = new List<Point>();
        /// <summary>
        /// Область еды увеличивающий хвост на 6 звеньев хвост.
        /// </summary>
        public List<Point> FieldFoodAddTailProperty { get { return FieldFoodAddTail; } }
        /// <summary>
        /// Центр еды увеличивающий хвост на 6 звеньев хвост.
        /// </summary>
        private Point FoodAddTail = new Point(-10, -10);

        /// <summary>
        /// Область яда.
        /// </summary>
        private List<Point> FieldToxin = new List<Point>();
        /// <summary>
        /// Область яда.
        /// </summary>
        public List<Point> FieldToxinProperty { get { return FieldToxin; } }
        /// <summary>
        /// Центр яда.
        /// </summary>
        private Point Toxin = new Point(-10, -10);

        Brush ColorUsualFood = Brushes.Red;
        Brush ColorDestroyTailFood = Brushes.Yellow;
        Brush ColorAddTailFood = Brushes.Brown;
        Brush ColorToxin = Brushes.DarkGoldenrod;

        /// <summary>
        ///  Типы (виды) еды.
        /// </summary>
        public enum TypeFood
        {
            UsualFood,
            DestroyTailFood,
            AddTailFood,
            Toxin
        }

        /// <summary>
        /// Заполнение области с едой.
        /// </summary>
        /// <param name="centreFood">Центр еды.</param>
        /// <param name="fieldFood">Область еды.</param>
        private void CompletionFieldFood(Point centreFood, List<Point> fieldFood)
        {
            //Длинна области еды.
            int LengthFood = 14;
            fieldFood.Clear();
            for (int i = centreFood.X - LengthFood / 2; i < centreFood.X + LengthFood / 2; i++)
            {
                for (int j = centreFood.Y - LengthFood / 2; j < centreFood.Y + LengthFood / 2; j++)
                {
                    // Точка из области еды.
                    Point buf = new Point(i, j);
                    fieldFood.Add(buf);
                }
            }
        }

        /// <summary>
        /// Создание новой точки с едой.
        /// </summary>
        /// <param name="p1">Точка ограничения области игры.</param>
        /// <param name="p2">Точка ограничения области игры.</param>
        /// <param name="p3">Точка ограничения области игры.</param>
        /// <param name="p4">Точка ограничения области игры.</param>
        /// <param name="snake">Змейка</param>
        /// <param name="type">Тип еды.</param>
        /// <param name="gr">Поверхность рисования.</param>
        public void GetNewFood(Point p1, Point p2, Point p3, Point p4, List<Point> snake, TypeFood type, Graphics gr)
        {
            // Случайная координата X для новой точки еды.
            int X = 0;
            // Случайная координата Y для новой точки еды.
            int Y = 0;
            GetNewCoordinates(p1, p2, p3, p4, snake, type,  out X, out Y);
            CreateFood(X, Y, type, gr);

        }

        /// <summary>
        /// Получение новых координат для еды.
        /// </summary>
        /// <param name="p1">Точка ограничения области игры.</param>
        /// <param name="p2">Точка ограничения области игры.</param>
        /// <param name="p3">Точка ограничения области игры.</param>
        /// <param name="p4">Точка ограничения области игры.</param>
        /// <param name="snake">Змейка</param>
        /// <param name="type">Тип еды.</param>
        /// <param name="X">Координата х.</param>
        /// <param name="Y">Координата у.</param>
        private void GetNewCoordinates(Point p1, Point p2, Point p3, Point p4, List<Point> snake, TypeFood type, out int X, out int Y)
        {
            // Случайная координата X для новой точки еды.
            X = 0;
            // Случайная координата Y для новой точки еды.
            Y = 0;
            // Проверка на выход из цикла
            bool Check;
            // Случайная велечина для координаты Х.
            Random RndX = new Random();
            // Случайная велечина для координаты Y.
            Random RndY = new Random();
            // Отступ от края для еды.
            int OffsetFood = 10;
            // Количество рандомов для координаты Х.
            int CountRandomX = 20;
            // Количество рандомов для координаты Y.
            int CountRandomY = 50;

            if(type == TypeFood.DestroyTailFood)
            {
                CountRandomX = 30;
                CountRandomY = 60;
            }
            else if (type == TypeFood.AddTailFood)
            {
                CountRandomX = 60;
                CountRandomY = 30;
            }

            do
            {
                Check = false;

                for (int i = 0; i < CountRandomX; i++)
                {
                    X = RndX.Next(p3.X + OffsetFood, p4.X - OffsetFood);
                }

                for (int i = 0; i < CountRandomY; i++)
                {
                    Y = RndY.Next(p3.Y + OffsetFood, p1.Y - OffsetFood);
                }

                foreach (Point el in snake)
                {
                    if (el.X == X && el.Y == Y)
                    {
                        Check = true; continue;
                    }

                }

            } while (Check);
        }

        /// <summary>
        /// Создание и отрисовка новой еды.
        /// </summary>
        /// <param name="x">Координата х.</param>
        /// <param name="y">Координата у.</param>
        /// <param name="type">Тип еды.</param>
        /// <param name="gr">Поверхность рисования.</param>
        private void CreateFood(int x, int y, TypeFood type, Graphics gr)
        {
            switch (type)
            {
                case (TypeFood.UsualFood):
                    UsualFood = new Point(x, y);
                    CompletionFieldFood(UsualFood, FieldUsualFood);
                    DrawFood(gr);
                    break;
                case (TypeFood.DestroyTailFood):
                    FoodDestroyTail = new Point(x, y);
                    CompletionFieldFood(FoodDestroyTail, FieldFoodDestroyTail);
                    DrawFood(gr);
                    break;
                case (TypeFood.AddTailFood):
                    FoodAddTail = new Point(x, y);
                    CompletionFieldFood(FoodAddTail, FieldFoodAddTail);
                    DrawFood(gr);
                    break;
                case (TypeFood.Toxin):
                    Toxin = new Point(x, y);
                    CompletionFieldFood(Toxin, FieldToxin);
                    DrawFood(gr);
                    break;
                default: break;
            }    
        }
        /// <summary>
        /// Удалить область еды уничтажающей хвост.
        /// </summary>
        public void ClearFoodDestroyTail()
        {
            FoodDestroyTail = new Point(-10, -10);
            FieldFoodDestroyTail.Clear();
        }
        /// <summary>
        /// Удалить область еды добавляющей хвосту 6 звеньев.
        /// </summary>
        public void ClearFoodAddTail()
        {
            FoodAddTail = new Point(-10, -10);
            FieldFoodAddTail.Clear();
        }

        /// <summary>
        /// Удалить яд.
        /// </summary>
        public void ClearToxin()
        {
            Toxin = new Point(-10, -10);
            FieldToxin.Clear();
        }

        /// <summary>
        /// Интервал появления и время жизни еды.
        /// </summary>
        /// <param name="seconds">Текущие игрвовое время.</param>
        /// <param name="p1">Точка ограничения области игры.</param>
        /// <param name="p2">Точка ограничения области игры.</param>
        /// <param name="p3">Точка ограничения области игры.</param>
        /// <param name="p4">Точка ограничения области игры.</param>
        /// <param name="snake">Змейка</param>
        /// <param name="gr">Поверхность рисования.</param>
        public void TimeLiveFood (int seconds, Point p1, Point p2, Point p3, Point p4, List<Point> snake, Graphics gr )
        {
            // Интервал появления еды уничтожающей хвост.
            int IntervalOfOccurrenceDestroyFood = 55;
            // Время жизни еды уничтожающей хвост.
            int TimeLiveDestroyFood = 5;

            // Интервал появления еды добавляющей 6 звеньев хвосту.
            int IntervalOfOccurrenceAddFood = 25;
            //Время жизни еды добавляющей 6 звеньев хвосту.
            int TimeLiveAddFood = 5;

            // Интервал появления яда.
            int IntervalOfOccurrenceToxin = 10;
            //Время жизни яда.
            int TimeLiveToxin = 8;

            if (seconds != 0)
            {
                if (seconds % IntervalOfOccurrenceDestroyFood == 0)
                {
                    GetNewFood(p1, p2, p3, p4, snake, TypeFood.DestroyTailFood, gr);
                }
                else if (seconds % TimeLiveDestroyFood == 0)
                {
                    ClearFoodDestroyTail();
                }
                if (seconds % IntervalOfOccurrenceAddFood == 0)
                {
                    GetNewFood(p1, p2, p3, p4, snake, TypeFood.AddTailFood, gr);
                }
                else if (seconds % TimeLiveAddFood == 0)
                {
                    ClearFoodAddTail();
                }

                if (seconds % IntervalOfOccurrenceToxin == 0)
                {
                    GetNewFood(p1, p2, p3, p4, snake, TypeFood.Toxin, gr);
                }
                else if (seconds % TimeLiveToxin == 0)
                {
                    ClearToxin();
                }
            }
        }

        /// <summary>
        /// Отрисовать еду.
        /// </summary>
        /// <param name="gr"> Поверхность рисования.</param>
        public void DrawFood(Graphics gr)
        {
            // Размер (точки) еды.
            int SizePoint = 10;
            gr.FillEllipse(ColorUsualFood, UsualFood.X - SizePoint / 2, UsualFood.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(ColorDestroyTailFood, FoodDestroyTail.X - SizePoint / 2, FoodDestroyTail.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(ColorAddTailFood, FoodAddTail.X - SizePoint / 2, FoodAddTail.Y - SizePoint / 2, SizePoint, SizePoint);
            gr.FillEllipse(ColorToxin, Toxin.X - SizePoint / 2, Toxin.Y - SizePoint / 2, SizePoint, SizePoint);
        }
    }
}
